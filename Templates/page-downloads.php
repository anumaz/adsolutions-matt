<?php /*
Template Name: Download page
*/
$count;
?>

<?php get_header(); ?>

<div id="main_content">
    <div id="left_area">
        <?php
        query_posts(
            array(
                'post_type' => 'download',
				'post_per_page' => 5
            ));?>
        <?php if (have_posts()): while (have_posts()): the_post();
            	$link = get_field('link');
				$size = get_field('size');
				$version = get_field('version');
				$description = get_field('description');
            ?>
			
            <br />
                <strong>File name: </strong><?php echo $link['title']; ?><br />
                <strong><a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" target="_blank">Click to download</a></strong><br />
                <strong>Size: </strong><?php echo $size ?><br />
                <strong>Version: </strong><?php echo $version ?><br />
                <strong>Description: </strong><?php echo $description ?><br />
                <br />
                <img alt="Colored Bar" src="http://anthonydupont.net/labotary/wp-content/uploads/2014/02/coloredbar.png" /><br />
            <br />
			<?php $count++; ?>
            <?php endwhile; ?>

        <?php else: ?>
		<strong>No downloads available.</strong><br />
        <?php endif; ?>

        <?php wp_reset_query(); ?>
            <!-- end #main_content -->
			<?php if ($count > 5) { echo 'Please click' ?><a href="labotary/fulldownloads"> this link </a><?php echo 'to see a full list of downloads'; } ?> 

	</div>
	<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>


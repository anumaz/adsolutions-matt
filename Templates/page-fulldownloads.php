<?php /*
Template Name: Full Download page
*/
?>

<div id="main_content">
    <div id="left_area">
        <?php
        query_posts(
            array(
                'post_type' => 'download',
            )); ?>
		<strong>---<a href="/labotary/download">Go back to website.</a></strong><br />
        <?php if (have_posts()): while (have_posts()): the_post();
            	$link = get_field('link');
            ?>
			<center>
            <br />
                <strong>File name: </strong><?php echo $link['title']; ?><br />
                <strong><a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" target="_blank">Click to download</a></strong><br />
                ----------------------------------<br /></center>
            <?php endwhile; ?>

        <?php else: ?>
		<strong>No downloads available.</strong><br />
        <?php endif; ?>

        <?php wp_reset_query(); ?>
            <!-- end #main_content -->

	</div>
</div>



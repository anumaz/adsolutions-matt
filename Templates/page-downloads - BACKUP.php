<?php /*
Template Name: Download page
*/?>

<?php get_header(); ?>

    <div id="main_content" class="clearfix">
        <div id="left_area">
            <?php
            $download_list = new WP_Query('post_type=download');
            while ( $download_list->have_posts() ) : $download_list->the_post();

                $file_name = get_field('file_name');
                $link = get_field('link');
                $size = get_field('size');
                $version = get_field('version');
                $description = get_field('description');
                $linkClean = the_field('link');

                var_dump($link['url']);
                ?>
<!--                <br />
                <strong>File name: </strong><?php /*echo $file_name */?><br />
                <strong>Click to download: </strong><a href="<?php /*echo $linkClean['url']; */?>" title="<?php /*echo $link['title']; */?>" target="_blank"><?php /*echo $link['title']; */?></a><br />
                <strong>Size: </strong><?php /*echo $size */?><br />
                <strong>Version: </strong><?php /*echo $version */?><br />
                <strong>Description: </strong><?php /*echo $description */?><br />
                <br />
                <img alt="Colored Bar" src="http://anthonydupont.net/labotary/wp-content/uploads/2014/02/coloredbar.png" /><br />
                <br />
-->            <?php endwhile; ?>


        </div> <!-- end #left_area -->

        <?php get_sidebar(); ?>
    </div> <!-- end #main_content -->

<?php get_footer(); ?>